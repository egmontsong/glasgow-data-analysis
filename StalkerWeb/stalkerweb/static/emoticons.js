/**
 * Created by Tianlong Song on 17/05/15.
 */
//alert(2);
window.addEventListener('load', function() {
    //alert(1);
    console.log(emotionUsage);
   $(function emotion_range() {
        $('#emoticons_range').highcharts({
            chart: {
                type: 'columnrange',
                inverted: true
            },

            title: {
                text: 'Polarity range against type of emoticons usage'
            },

            subtitle: {
                text: 'Unicode Emoticons'
            },

            xAxis: {
                type: 'category'
            },

            yAxis: {
                title: {
                    text: 'Polarity (Negative <-> Positive)'
                }
            },

            tooltip: {
                valueSuffix: ''
            },

            plotOptions: {
                columnrange: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y;
                        }
                    }
                }
            },

            legend: {
                enabled: true
            },

            series: [{
                name: 'Polarity',
                data: $(emotionRange),
                color: Highcharts.getOptions().colors[7],
                dataLabels: {
                    format: '{point.y}' // one decimal
                }
            }]
        });
    });

    $(function emotion_usage() {
        $('#emoticons_amount').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Emoticons Usage'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Usage'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Number of occurrence: <b>{point.y} times</b>'
            },
            series: [{
                name: 'Usage',
                data: $(emotionUsage),
                color: Highcharts.getOptions().colors[7],
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: Highcharts.getOptions().colors[7],
                    align: 'left',
                    format: '{point.y}', // one decimal
                    y: 0, // 10 pixels down from the top
                    style: {
                        fontSize: '13`px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
});
