function loadchart(days,symbol, day_number) {
    var symbolArray=[];
    for (var i = 0; i < 24; i++) {
            symbolArray.push({
                y: 0.3,
                marker: {
                    symbol: 'url('+symbol[day_number][i][0].value+')',
                    height: 30,
                    width: 30
                }
            });
        }

    $(function () {
        $('#weather_line').highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Hourly Average Polarity'
            },
            subtitle: {
                text: 'Twitter'
            },
            xAxis: {
                title: {
                    text: 'Time'
                },
                categories: ['0', '1', '2', '3', '4', '5',
                    '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23']
            },
            yAxis: {
                title: {
                    text: 'Polarity'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Weather',
                marker: {
                    symbol: 'square'
                },
                data: symbolArray

            }, {
                name: 'Polarity',
                marker: {
                    symbol: 'diamond'
                },
                data: days[day_number]
            }]
        });
    });
}
