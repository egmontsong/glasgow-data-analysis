/**
 * Created by Tianlong Song on 27/05/15.
 */
window.addEventListener('load', function() {

    console.log(wordAverage);
    $(function word_average() {
        $('#word_average').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Average Polarity of Word'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: -0.5,
                title: {
                    text: 'Average Polarity'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Average Polarity: <b>{point.y}</b>'
            },
            series: [{
                name: 'Average Polarity',
                data: $(wordAverage),
                color: Highcharts.getOptions().colors[7],
                negativeColor: Highcharts.getOptions().colors[8],
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: Highcharts.getOptions().colors[1],
                    align: 'left',
                    format: '{point.y}', // one decimal
                    y: 0, // 10 pixels down from the top
                    style: {
                        fontSize: '13`px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
});