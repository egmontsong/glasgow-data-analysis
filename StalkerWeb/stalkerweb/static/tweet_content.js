/**
 * Created by charlene_chen on 18/05/2015.
 */
window.addEventListener('load', function() {
    //alert(1);
    console.log(hashtags);
    $(function tweet_content() {
        $('#tweetcontent').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of Tags of Weekday'
            },
            xAxis: {
                type: 'Per',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of hashtags'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Number of occurrence: <b>{point.y} times</b>'
            },
            series: [{
                data: $(hashtags),
                color: Highcharts.getOptions().colors[0],
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: Highcharts.getOptions().colors[0],
                    align: 'left',
                    format: '{point.y}', // one decimal
                    y: 0, // 10 pixels down from the top
                    style: {
                        fontSize: '13`px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });

    $(function tweet_meida() {
    $('#tweetmedia').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Percentage of Tweets with media'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            data: $(media)
        }]
    });
});
});