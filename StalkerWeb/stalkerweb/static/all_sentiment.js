/**
 * Created by Tianlong Song on 19/05/15.
 */

window.addEventListener('load', function() {
    console.log(sentimentData);
    $(function all_sentiment() {
        $('#all_sentiment').highcharts({
            chart: {
                type: 'bubble',
                zoomType: 'xy'
            },
            xAxis: {
                title: {
                    text: 'Polarity'
                }
            },
            yAxis: {
                title: {
                    text: 'Subjectivity'
                }
            },
            title: {
                text: 'Sentiment Distribution in Glasgow'
            },
            subtitle: {
                text: 'Source: Twitter  2015 18-April -- 8-May'
            },
            series: [{
                data: $(sentimentData),
                displayNegative: true,
                color: Highcharts.getOptions().colors[8],
                negativeColor: Highcharts.getOptions().colors[7],
                zThreshold: 2000,
                maxSize: "18%",
                minSize: "20"
                //zones: [{
					//value: 0,
					//color: '#f7a35c'
                //}, {
					//value: 1000,
					//color: '#7cb5ec'
                //}, {
                //    value: 2000,
					//color: '#90ed7d'
                //},
                //]
            }]
        });
    });
});

