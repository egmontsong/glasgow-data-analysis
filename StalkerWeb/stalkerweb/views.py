from pyramid.view import view_config
import os
from pyramid.response import Response
from postprocessors.sentimentprocessing import Sentiment_processing
from postprocessors.emoticonprocessing import Emoticons_processing
from postprocessors.wordcloudprocessing import Word_cloud_processor
from postprocessors.weatherprocessing import get_weather
import postprocessors.couchdbviewresults


@view_config(route_name='home', renderer='templates/home.jinja2')
def view_home(request):
    return {'name': 'team'}

@view_config(route_name='word_cloud', renderer='templates/word_cloud.jinja2')
def view_wordcloud(request):
    wordcloud_processing = Word_cloud_processor()
    import json
    return {'word_sentiment_range': json.dumps(wordcloud_processing.get_all_range()),'json1': json.dumps,
            'word_cloud': json.dumps(wordcloud_processing.get_all_weather_words()), 'json': json.dumps}

@view_config(route_name='all_sentiment', renderer='templates/all_sentiment.jinja2')
def view_allsentiment(request):
    sentiment_processing = Sentiment_processing()
    return {'sentiment':  sentiment_processing.get_all_sentiment()}

@view_config(route_name='emoticons', renderer='templates/emoticons.jinja2')
def emoticons(request):
    emoticons_processing = Emoticons_processing()
    import json
    return {'emoticons': json.dumps(emoticons_processing.get_all_emotions()), 'json':json.dumps,
            'emoticons_range': json.dumps(emoticons_processing.get_all_range()),'json1':json.dumps}

@view_config(route_name='tweet_content', renderer='templates/tweet_content.jinja2')
def tweetcontent_view(request):
    import postprocessors.couchdbviewresults
    import json
    return {'hashtags': json.dumps(postprocessors.couchdbviewresults.get_weekdayHashTags()),'json' : json.dumps,
            'media' : json.dumps(postprocessors.couchdbviewresults.count_media()), 'json1':json.dumps}


@view_config(route_name='weather', renderer='templates/weather.jinja2')
def weather_view(request):
    days_symbols = get_weather()
    import json
    return {'hours_emotion': days_symbols, 'jsonify':json.dumps}

# @view_config(route_name='dateHashTags', renderer='json')
# def json_data(request):
#     # return [["Mon", 12], ["Tue", 69], ["Wed", 0], ["Thu", 0], ["Fri", 0], ["Sat", 0], ["Fri", 0]]
#     # print os.path.dirname(os.path.abspath(__file__))
#     with open(os.path.dirname(os.path.abspath(__file__))+'/static/dateHashTags.json') as f:
#         data = f.read()
#     # print(data)
#     return Response(data)
#
# @view_config(route_name='mediacount', renderer='json')
# def media_data(request):
#     with open(os.path.dirname(os.path.abspath(__file__))+'/static/mediacount.json') as f:
#         data = f.read()
#     # print(data)
#     return Response(data)
