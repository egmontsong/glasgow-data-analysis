from pyramid.config import Configurator
from pyramid.events import subscriber
from pyramid.events import BeforeRender
import json



@subscriber(BeforeRender)
def add_global(event):
    event['jsonify'] = json.dumps

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    # config.include('pyramid_jin')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('word_cloud', '/word_cloud')
    config.add_route('all_sentiment', '/all_sentiment')
    config.add_route('emoticons', '/emoticons')
    config.add_route('tweet_content', '/tweet_content')
    config.add_route('weather', '/weather')
    config.add_route('dateHashTags', '/dateHashTags.json')
    config.add_route('mediacount', '/mediacount.json')


    # User section
    config.add_route('tweeter.list', '/tweeter/list')
    config.add_route('tweeter.geo_profile', '/tweeter/profile/{id}')

    config.scan()
    return config.make_wsgi_app()
