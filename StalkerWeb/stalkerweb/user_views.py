__author__ = 'shadyrafehi'

from pyramid.view import view_config
from postprocessors import geoprocessing

def name_anonymiser(name):
    return " ".join([n[0] + ('*' * len(n[1:-1])) + n[-1] for n in name.split(' ')])

def anonymise_id(user_id):
    return ''.join([chr(57+i+ord(c)) for i, c in enumerate(str(user_id))][-1::-1])

def unanonymise_id(user_id):
    return ''.join([chr(ord(c)-57-i) for i, c in enumerate(str(user_id)[-1::-1])])

@view_config(route_name='tweeter.list', renderer='templates/tweeter/tweeter_list.jinja2')
def user_list(request):
    top_tweeters = geoprocessing.top_tweeters()

    output = []
    for tweeter in top_tweeters:
        output.append(dict(
            id=anonymise_id(tweeter['_id']),
            username=name_anonymiser(tweeter['username']),
            tweet_count=len(tweeter['points']),
            distance_travelled=round(tweeter['distances']['total']/1000, 1),
            regions=list(set([p['region'] for p in tweeter['points']]))
            )
        )

    return {'tweeters': output}

@view_config(route_name='tweeter.geo_profile', renderer='templates/tweeter/tweeter_geo_profile.jinja2')
def geo_profile(request):
    _id = unanonymise_id(request.matchdict.get('id'))
    profile = geoprocessing.geo_profile(_id)
    profile['info']['username'] = name_anonymiser(profile['info']['username'])
    profile['info']['name'] = name_anonymiser(profile['info']['name'])
    return {
        'geodata': profile['data'],
        'tweeter': profile['info']
    }