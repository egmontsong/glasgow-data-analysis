from NeCTAR.base import NeCTARBase
import NeCTAR.instance
from NeCTAR.event import Event

__author__ = 'shadyrafehi'


class Volume(NeCTARBase):

    device = '/dev/vdc'

    def __init__(self, id):
        self._map_id = id

    @property
    def _volume(self):
        return super(self.__class__, self).mapping[self._map_id]

    @property
    def id(self):
        return self._volume.id

    @property
    def size(self):
        return self._volume.size

    @property
    def owner(self):
        attach_data = self._volume.attach_data
        if attach_data is None:
            return None
        return NeCTAR.instance.Instance.by_id(self._volume.attach_data.instance_id)

    @property
    def status(self):
        return self._volume.status

    @classmethod
    def all(cls, ids=None):
        volumes = []
        for v in super(cls, cls).connection.get_all_volumes(volume_ids=None):
            super(cls, cls).mapping[v.id] = v
            volumes.append(Volume(v.id))
        return volumes

    @classmethod
    def by_id(cls, id, allow_cached=True):
        if allow_cached:
            volume = super(cls,cls).mapping.get(id)
            if volume is not None:
                return Volume(volume.id)
        for volume in cls.all(id):
            if volume.id == id:
                return volume
        return None

    def attach(self, instance):
        super(self.__class__, self).connection.attach_volume(self.id, instance.id, Volume.device)

        def task():
            while self.owner is None:
                self.refresh()
            self.owner.refresh()
        return Event(task, "Attach:{}".format(self))

    def detach(self):
        if self.owner is None:
            return Event(lambda:True)
        owner = self.owner
        super(self.__class__, self).connection.detach_volume(self.id)

        def task():
            while self.status == 'in-use':
                self.refresh()
            while owner.volume != None:
                owner.refresh()
        return Event(task, "Detach:{}".format(self))

    @classmethod
    def new(cls, size):
        volume_data = super(cls,cls).connection.create_volume(size, 'tasmania')
        super(cls,cls).mapping[volume_data.id] = volume_data
        volume = Volume(volume_data.id)

        def task():
            while Volume.by_id(volume.id, allow_cached=False).status == 'creating':
                pass
            volume.refresh()
        Event(task, "New:{}".format(volume))

        return volume

    def delete(self, wait=False):
        super(self.__class__, self).connection.delete_volume(self.id)

        # Add Event
        def task():
            while Volume.by_id(self.id, allow_cached=False):
                pass
        return Event(task, "Terminate:{}".format(self))

    def refresh(self):
        Volume.by_id(self.id, allow_cached=False)

    def __repr__(self):
        owner_id = self.owner.id if self.owner else None
        return 'Volume(Id={}, Owner={}, Size={})'.format(self.id, owner_id, self.size)