__author__ = 'shadyrafehi'

from NeCTAR.base import NeCTARBase


class Image(NeCTARBase):

    @classmethod
    def all(cls):
        return super(cls, cls).connection.get_all_images()

    @classmethod
    def by_id(cls, id):
        return super(cls, cls).connection.get_image(id)