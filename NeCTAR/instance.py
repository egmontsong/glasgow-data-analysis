from NeCTAR.base import NeCTARBase
import NeCTAR.volume
from NeCTAR.event import Event
from NeCTAR import Size

__author__ = 'shadyrafehi'


class Instance(NeCTARBase):

    def __init__(self, instance_id):
        self._map_id = instance_id

    @property
    def _instance(self):
        return super(self.__class__, self).mapping[self._map_id]

    @property
    def id(self):
        return self._instance.id

    @property
    def type(self):
        return self._instance.instance_type

    @property
    def ip_address(self):
        return self._instance.ip_address

    @property
    def name(self):
        return self._instance.private_dns_name

    @property
    def volume(self):
        mapping = self._instance.block_device_mapping
        if mapping is None:
            return None
        volume_id = mapping.get(NeCTAR.volume.Volume.device).volume_id
        if volume_id in super(self.__class__, self).mapping:
            return NeCTAR.volume.Volume(volume_id)
        return NeCTAR.volume.Volume.by_id(volume_id)

    @property
    def tags(self):
        return self._instance.tags or dict()

    def attach_volume(self, volume):
        return volume.attach(self)

    def detach_volume(self):
        volume = self.volume
        if volume is None:
            return
        return volume.detach()

    def refresh(self):
        Instance.by_id(self.id, allow_cached=False)

    @classmethod
    def all(cls, ids=None):

        try:
            reservations = super(cls, cls).connection.get_all_instances(instance_ids=ids)
        except:
            reservations = []

        instances = []
        for reservation in reservations:
            inst = reservation.instances[0]
            super(cls, cls).mapping[inst.id] = inst
            instances.append(cls(inst.id))
        return instances

    @classmethod
    def by_id(cls, id, allow_cached=True):
        if allow_cached:
            instance = super(cls,cls).mapping.get(id)
            if instance is not None:
                return Instance(instance.id)
        for instance in cls.all(id):
            if instance.id == id:
                return instance
        return None

    @classmethod
    def by_tag(cls, tag, value, comparator=lambda x, y: x == y):
        instances = []
        for instance in cls.all():
            if comparator(instance.tags.get(tag, ''), value):
                instances.append(instance)
        return instances

    @classmethod
    def by_name(cls, name):
        for instance in cls.all():
            if instance.name == name:
                return instance
        return None

    @classmethod
    def new(cls,
            name,
            size = Size.small,
            image_id = NeCTARBase.default_image,
            key_name = NeCTARBase.default_key_name,
            security_groups=NeCTARBase.default_security_groups):
        reservation = super(cls, cls).connection.run_instances(
            image_id,
            key_name=key_name,
            instance_type=size,
            security_groups=security_groups,
        )
        instance_data = reservation.instances[0]
        super(cls,cls).mapping[instance_data.id] = instance_data
        instance = Instance(instance_data.id)

        # Add Event
        def task():
            while Instance.by_id(instance_data.id, allow_cached=False).state.name == 'pending':
                pass
            super(cls, cls).connection.create_tags([instance.id], dict(name=name))
            instance.refresh()
        Event(task, "New:{}".format(instance))

        return instance

    def terminate(self, wait=False):
        super(self.__class__, self).connection.terminate_instances(instance_ids=[self.id])

        # Add Event
        def task():
            while Instance.by_id(self.id, allow_cached=False):
                pass
        return Event(task, "Terminate:{}".format(self))

    @property
    def state(self):
        return self._instance._state

    def __repr__(self):
        short_name = self._instance.private_dns_name
        if len(short_name) > 24: short_name = short_name[:24] + '...'
        return 'Instance(Id={}, Name={})'.format(self._instance.id, short_name)


