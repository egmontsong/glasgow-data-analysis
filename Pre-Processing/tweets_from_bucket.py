
RELATIVE_PATH = '/home/ubuntu/app/Pre-Processing'

def download_files():
	import urllib, json
	filenames = json.load(open(RELATIVE_PATH + 'filenames.json'))
	url = 'https://swift.rc.nectar.org.au:8888/v1/AUTH_21093552c0cb40a5a078c74fb820b3ec/tweetdata/{}'

	for i, filename in enumerate(filenames):
		if i%50 == 0:
			print i
		with open('files/' + filename, 'w') as f:
			f.write(urllib.urlopen(url.format(filename.replace('%', '%25'))).read())



def merge_files(output_name):
	import glob, json
	filenames = glob.glob(RELATIVE_PATH + 'files/*')
	all_json = []

	def write_to_file(name, data):
		with open(name, 'w') as f:
			json.dump(data, f)

	for i, filename in enumerate(filenames):
		if i > 0 and i%1000 == 0:
			print i, '/', len(filenames)
			write_to_file('{}merged/{}-{}'.format(RELATIVE_PATH, i, output_name), all_json)
			all_json = []
		all_json += json.load(open(filename))

	write_to_file('{}-{}'.format(i,output_name), all_json)


def clean_tweets(filepath, outputpath):
	import json
	with open(filepath) as f:
		tweets = json.load(f)

	root_keys = ['entities', 'retweet_count', 'coordinates', 'id_str', 'text', 'in_reply_to_status_id','favorite_count','retweeted','timestamp_ms','in_reply_to_screen_name','in_reply_to_user_id','user']
	user_keys = ['id_str', 'verified', 'followers_count', 'statuses_count', 'description', 'friends_count', 'profile_image_url', 'screen_name', 'lang', 'name', 'created_at', 'following_count']
	cleaned_tweets = []
	for tweet in tweets:
		tweet = {x: y for x,y in tweet.items() if x in root_keys}
		tweet['user'] = {x: y for x,y in tweet['user'].items() if x in user_keys}
		cleaned_tweets.append(tweet)

	with open(outputpath, 'w') as f:
		tweets = json.dump(cleaned_tweets, f)


def remove_duplicates():
	import glob, json
	filenames = glob.glob(RELATIVE_PATH + 'cleaned/*')

	tracker = set()
	tweets_dict = {}
	tweets = []

	print 'loading tweets'
	for filename in filenames:
		print 'loading', filename, '...'
		tweets += json.load(open(filename))
		print 'loaded', filename
	print 'loaded tweets'

	filtered_tweets = []
	for i, tweet in enumerate(tweets):

		if i%1000 == 0:
			print i, '/', len(tweets)

		tid = int(tweet['id_str'])
		if tid not in tracker:
			filtered_tweets.append(tweet)
			tweets_dict[tid] = tweet
		else:
			assert tweet == tweets_dict[tid]
		tracker.add(tid)

	with open('tweets-filtered.json', 'w') as f:
		json.dump(filtered_tweets, f)

	print 'Unique: {}, Duplicates: {}'.format(len(filtered_tweets), len(tweets) - len(filtered_tweets))


if __name__ == '__main__':
	download_files()
	merge_files('raw_tweets.json')

	import glob
	merged_filenames = glob.glob(RELATIVE_PATH + 'merged/*')
	for i, filename in enumerate(merged_filenames, 1):
		print 'cleaning', filename
		clean_tweets(filename, RELATIVE_PATH + 'cleaned/{}-tweets.json'.format(i))

	remove_duplicates()


