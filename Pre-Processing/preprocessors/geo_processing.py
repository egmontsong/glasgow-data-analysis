__author__ = 'shadyrafehi'

import utm
import math

def bounding_box(coord, southwest, northeast):
    return (southwest[1] <= coord[1] <= northeast[1]) and \
           (southwest[0] <= coord[0] <= northeast[0])

class Geotagger(object):

    def __init__(self, tweets, regions, sw_bound, ne_bound):
        self.tweets = tweets
        self.regions = regions
        self.user_profiles = {}
        self.sw_bound = sw_bound
        self.ne_bound = ne_bound

    def process(self):
        tracked = set()

        print 'Loading tweets'

        outside = 0
        no_coord = 0
        not_found = 0
        found = 0
        duplicate = 0
        to_return = []

        print 'Parsing tweets'

        for i, tweet in enumerate(self.tweets):
            tweet_id = tweet['id_str']
            if tweet_id in tracked:
                continue
            tracked.add(tweet_id)
            tweet['_id'] = tweet_id

            if i%1000 == 0:
                print i, '/', len(self.tweets), 'parsed'

            if 'custom' not in tweet:
                tweet['custom'] = {}

            tweet['custom']['region'] = None

            coord = None
            try:
                lng, lat = tweet['coordinates']['coordinates']
                coord = [lat,lng]
            except:
                # print 'Skipping: no coordinates'
                no_coord += 1

            if coord:
                ''' Check to see if coord in glasgow '''
                if not bounding_box(coord, self.sw_bound, self.ne_bound):
                    # print 'Skipping: outside of region'
                    outside += 1
                    continue

                ''' Check to see which city tweet is from '''
                for region in self.regions:
                    if bounding_box(coord, region['southwest'], region['northeast']):

                        town = region['town']
                        user_id = tweet['user']['id_str']

                        tweet['custom']['region'] = town

                        if user_id not in self.user_profiles:
                            self.user_profiles[user_id] = {
                                '_id': user_id,
                                'username': tweet['user']['screen_name'],
                                'name': tweet['user']['name'],
                                'description': tweet['user']['description'],
                                'followers': tweet['user']['followers_count'],
                                'following': tweet['user']['friends_count'],
                                'points': []
                            }

                        self.user_profiles[user_id]['points'].append({
                            'tweet_id': tweet_id,
                            'region': town,
                            'coordinate': coord,
                            'timestamp': tweet['custom']['time']
                        })
                        found += 1
                        break

                ''' Did we find a region? '''
                if tweet['custom']['region'] is None:
                    not_found += 1

            to_return.append(tweet)

        print "(no_coord, outside, not_found, found, duplicate) = ({}, {}, {}, {}, {})".format(no_coord, outside, not_found, found, duplicate)

        print 'Calculating distances/inserting into DB'

        for i, (k,v) in enumerate(self.user_profiles.items()):

            if i%500 == 0:
                print i, '/', len(self.user_profiles), 'parsed'

            v['points'] = sorted(v['points'], key=lambda x: x['timestamp'])
            points = [list(utm.from_latlon(*p['coordinate']))[:2] for p in v['points']]
            distances = [math.hypot(points[i-1][0]-points[i][0], points[i-1][1]-points[i][1]) for i in xrange(1,len(points))]
            total_distance, avg_distance = sum(distances), sum(distances)/len(points)
            v['distances'] = dict(
                total=total_distance,
                average=avg_distance,
                points=distances
            )

        print 'Done'
        return to_return, self.user_profiles.values()

