__author__ = 'Tianlong Song'

import re

class Emoticons:

    def __init__(self):
        mycompile = lambda pat:  re.compile(pat,  re.UNICODE)

        NormalEyes = r'[:=]'
        Wink = r'[;]'
        NoseArea = r'(|o|O|-)'   ## rather tight precision, \S might be reasonable...
        HappyMouths = r'[D\)\]]'
        SadMouths = r'[\(\[]'
        Tongue = r'[pP]'

        Unicode_emoticon_happy = u'\ud83d[\ude00-\ude10]'
        Unicode_emoticon_kiss = u'\ud83d[\ude17-\ude1a]'
        Unicode_emoticon_tongue = u'\ud83d[\ude1b-\ude1d]'
        Unicode_emoticon_angry = u'\ud83d[\ude20-\ude21]'
        Unicode_emoticon_sad = u'\ud83d[\ude11-\ude16]'
        Unicode_emoticon_worried = u'\U0001f61e'
        Unicode_emoticon_sosad = u'\ud83d[\ude22-\ude29]'
        Unicode_emoticon_screaming = u'\ud83d[\ude31]'
        Unicode_emoticon_sleepy = u'\ud83d[\ude34-\ude35]'

        self.Happy_RE =  mycompile( '(\^_\^|' + NormalEyes + NoseArea + HappyMouths + ')')
        self.Sad_RE = mycompile(NormalEyes + NoseArea + SadMouths)
        self.Wink_RE = mycompile(Wink + NoseArea + HappyMouths)
        self.Tongue_RE = mycompile(NormalEyes + NoseArea + Tongue)
        self.E_Happy_RE = mycompile(Unicode_emoticon_happy)
        self.E_Sad_RE = mycompile(Unicode_emoticon_sad)
        self.E_Kiss_RE = mycompile(Unicode_emoticon_kiss)
        self.E_Tongue_RE = mycompile(Unicode_emoticon_tongue)
        self.E_Worried = mycompile(Unicode_emoticon_worried)
        self.E_sossad_RE = mycompile(Unicode_emoticon_sosad)
        self.E_Angry_RE = mycompile(Unicode_emoticon_angry)
        self.E_Screaming_RE = mycompile(Unicode_emoticon_screaming)
        self.E_Sleepy_RE = mycompile(Unicode_emoticon_sleepy)

    def analyze_tweet(self,sentences):
        emoticons = []
        for sentence in sentences:
            eh = self.E_Happy_RE.search(sentence)
            es = self.E_Sad_RE.search(sentence)
            ea = self.E_Angry_RE.search(sentence)
            esc = self.E_Screaming_RE.search(sentence)
            esl = self.E_Sleepy_RE.search(sentence)
            ess = self.E_sossad_RE.search(sentence)
            et = self.E_Tongue_RE.search(sentence)
            ek = self.E_Kiss_RE.search(sentence)
            ew = self.E_Worried.search(sentence)

            h = self.Happy_RE.search(sentence)
            s = self.Sad_RE.search(sentence)
            w = self.Wink_RE.search(sentence)
            t = self.Tongue_RE.search(sentence)

            if h or eh: emoticons.append("HAPPY")
            if s or es or ess: emoticons.append("SAD")
            if w: emoticons.append("WINK")
            if t or et: emoticons.append("TONGUE")
            if ea: emoticons.append("ANGRY")
            if esc: emoticons.append("SCREAMING")
            if esl: emoticons.append("SLEEPY")
            if ek: emoticons.append("KISS")
            if ew: emoticons.append("WORRIED")
        return emoticons


# if __name__=='__main__':
#        print list(u'[\U0001f622-\U0001f629]')
