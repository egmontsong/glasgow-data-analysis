__author__ = 'Tianlong Song'

from linguistics import Linguistic
from emoticons import Emoticons
from sentiment import Sentiment

class SemanticAnalyser:
    def __init__(self, tweets, weather_filename):
        self.tweets = tweets
        self.emoticons = Emoticons()
        self.sentiment = Sentiment()
        self.linguistic = Linguistic(weather_filename)

    def process(self):
        for i, tweet in enumerate(self.tweets):

            if i%100 == 0:
                print i, '/', len(self.tweets)

            text = tweet['text']

            sentences = Sentiment.tokenizer(text)

            # return sentiment value
            s_value = Sentiment.calculate_sentiment(sentences)
            key_words = self.linguistic.noun_word_extractor(sentences)
            weather_words = self.linguistic.weather_word_search(key_words)

            #return emoticons emotion
            e_value = self.emoticons.analyze_tweet(sentences)

            tweet['custom']['semantics'] = {
                "polarity": s_value[0],
                "subjectivity": s_value[1],
                "emotions": e_value,
                "key_words": key_words,
                "weather_words": weather_words
            }
