__author__ = 'Tianlong Song'

from textblob import TextBlob
from textblob.tokenizers import SentenceTokenizer
from textblob.sentiments import NaiveBayesAnalyzer
import re


class Sentiment(object):

    re1 = re.compile(r'@\w+')
    re2 = re.compile(r'#\w+')

    @staticmethod
    def tokenizer(text):
        text = Sentiment.re1.sub('', text)
        text = Sentiment.re2.sub('', text)
        tokenizer = SentenceTokenizer()
        blob = TextBlob(text, tokenizer=tokenizer)
        clean_text = blob.tokens
        return clean_text

    @staticmethod
    def pattern_analyser(sentence):
        blob_pattern = TextBlob(sentence)
        return blob_pattern.sentiment

    @staticmethod
    def bayes_analyser(sentence):
        blob_bayes = TextBlob(sentence, analyzer=NaiveBayesAnalyzer())
        return blob_bayes.sentiment

    @staticmethod
    def calculate_sentiment(sentences):
        sentiment_polarity = 0
        sentiment_subjectivity = 0

        size = len(sentences)
        for sentence in sentences:
            sentiment_polarity += Sentiment.pattern_analyser(sentence).polarity
            sentiment_subjectivity += Sentiment.pattern_analyser(sentence).subjectivity

        if size == 0:
            sentiment_polarity = sentiment_polarity
            sentiment_subjectivity = sentiment_subjectivity
        else:
            sentiment_polarity /= size
            sentiment_subjectivity /= size

        return sentiment_polarity, sentiment_subjectivity

# if __name__=='__main__':
#     import time
#     start_time = time.time()
#     sentiment = Sentiment()
#     tweet = "@AmberRuddhr @GregBarkerUK @bowers_jake Severe cuts to FiT &amp; complex/ risky CfD process pushing Greg's Big 60k out of market"
#     print tweet
#     sentences = sentiment.tokenizer(tweet)
#     value = sentiment.calculate_sentiment(sentences)
#     print value
#     end_time = time.time()
#     print end_time-start_time
#     # sentiment.sentiment_predictor(value.__getitem__(0),value.__getitem__(1),value.__getitem__(2),value.__getitem__(3))

