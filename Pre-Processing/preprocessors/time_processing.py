__author__ = 'Tingting Wen'

import datetime, pytz, tzlocal


class TimeDistribution(object):
    def __init__(self, tweets_list):
        self.tweets_list = tweets_list
        # self.local_dict = {}

    def process(self):
        for tweet in self.tweets_list:
            time_dict = self.utc_to_local(tweet)
            custom_dict = tweet['custom']
            custom_dict['time'] = {'date': self.get_date(time_dict),
                                   'hour': self.get_hour(time_dict),
                                   'minute': self.get_min(time_dict)}
            del tweet['timestamp_ms']
        return self.tweets_list

    # Convert timestamp into utc time
    def timestamp_to_utc(self, tweet_dict):
        timestamp = int(tweet_dict['timestamp_ms'])
        server_time = datetime.datetime.fromtimestamp(timestamp / 1e3)
        local_tz = pytz.timezone(str(tzlocal.get_localzone()))
        utc = server_time.replace(tzinfo=local_tz).astimezone(pytz.utc)
        return utc

    # Call timestamp_to_utc()
    # Convert utc time to glasgow time, and update the local time dictionary
    def utc_to_local(self, tweet_dict):
        utc = self.timestamp_to_utc(tweet_dict)
        local_tz = pytz.timezone('WET')
        local = utc.replace(tzinfo=pytz.utc).astimezone(local_tz)
        # Local time into a dict
        time_dict = {'year': local.year, 'month': local.month, 'date': local.day, 'hour': local.hour,
                     'minute': local.minute}
        # print time_dict
        return time_dict

    # Return an integer represents date, in format as 20150530
    def get_date(self, time_dict):
        date = '{}{}{}'.format(time_dict['year'], format(time_dict['month'], '02d'), format(time_dict['date'], '02d'))
        return int(date)

    # Return an two digits integer represents hour, in format as 05
    def get_hour(self, time_dict):
        return time_dict['hour']

    # Return an two digits integer represents minute, in format as 05
    def get_min(self, time_dict):
        return time_dict['minute']