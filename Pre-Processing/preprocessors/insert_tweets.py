__author__ = 'shadyrafehi'


import json
import couchdb
import time_processing

tweets = json.load(open('tweets-short.json'))
td = time_processing.TimeDistribution(tweets)
tweets = td.time_process()

couch = couchdb.Server()

try:
    couch.delete('test_db')
except couchdb.ResourceNotFound:
    pass
finally:
    db = couch.create('test_db')

for i, tweet in enumerate(tweets):
    if i % 500 == 0:
        print i, '/', len(tweets)
    db.save(tweet)