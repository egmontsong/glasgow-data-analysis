__author__ = 'shadyrafehi'

# TODO
import json
import couchdb
from preprocessors.time_processing import TimeDistribution
from preprocessors.geo_processing import Geotagger
from preprocessors.semantics_processing.processor import SemanticAnalyser


def chunks(l, n):
    n = max(1, n)
    return [l[i:i + n] for i in range(0, len(l), n)]

def add_weather():
    import couchdb
    import time
    from datetime import date
    import datetime
    import requests
    server = couchdb.Server('http://127.0.0.1:5984')
    try:
        db = server['weather']
    except:
        db = server.create('weather')

    position = {}
    date_start = date(2015, 4, 23)
    date_end = date.fromtimestamp(time.time())
    total_days = (date_end - date_start).days + 1 #inclusive 5 days
    print
    for day_number in range(total_days):
        current_date = date_start + datetime.timedelta(days=day_number)
        r = requests.get('http://api.worldweatheronline.com/premium/v1/past-weather.ashx?key=7ba0e9f195e3badb4569471e210df&q=G1&date=%s&tp=1&format=json' % (str(current_date)))
        db.save(r.json()['data'])

if __name__ == '__main__':
    print 'Loading tweets...'
    tweets = json.load(open('tweets-filtered.json'))
    print 'Tweets loaded'

    for tweet in tweets:
        tweet['custom'] = {}

    ''' Time processing '''
    time_distribution = TimeDistribution(tweets)
    tweets = time_distribution.process()

    ''' Geo processing '''
    sw_bound = [55.55, -4.64]
    ne_bound = [56.18, -3.43]
    regions = json.load(open('regions.json'))
    geotagger = Geotagger(tweets, regions, sw_bound, ne_bound)
    tweets, user_profiles = geotagger.process()

    ''' Sentiment processing '''
    semantic_analyser = SemanticAnalyser(tweets, 'weather.txt')
    semantic_analyser.process()

    ''' Database insertion '''
    couch = couchdb.Server()

    tweets_db_name = 'tweets'
    profile_db_name = 'user_profiles'

    try:
        couch.delete(tweets_db_name)
    except couchdb.ResourceNotFound:
        pass
    finally:
        tweets_db = couch.create(tweets_db_name)

    try:
        couch.delete(profile_db_name)
    except couchdb.ResourceNotFound:
        pass
    finally:
        user_db = couch.create(profile_db_name)

    with open('pp_tweets.json', 'w') as f:
        json.dump(tweets, f)

    with open('pp_profiles.json', 'w') as f:
        json.dump(user_profiles, f)

    tweet_chunks = chunks(tweets, 2000)
    for i, chunk in enumerate(tweet_chunks, 1):
        print i, '/', len(tweet_chunks), 'tweet chunks inserted'
        tweets_db.update(chunk)

    profile_chunks = chunks(user_profiles, 2000)
    for i, chunk in enumerate(profile_chunks, 1):
        print i, '/', len(profile_chunks), 'profile chunks inserted'
        user_db.update(chunk)

    add_weather()

