__author__ = 'shadyrafehi'

import datetime


class Logger(object):

    def __init__(self, name, filename):
        self.name = name
        self.filename = filename

    @property
    def file(self):
        return open(self.filename,'a')

    def log(self, message):
        timestamp = datetime.datetime.now().strftime('%-I:%M:%S %a %B %-d, %Y')
        output = '''\
------------------------------------------------------
[{name}] {timestamp}
------------------------------------------------------
{message}


'''
        with self.file as f:
            f.write(output.format(name=self.name, timestamp=timestamp, message=message))
