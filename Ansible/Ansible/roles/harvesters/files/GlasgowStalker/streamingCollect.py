__author__ = 'Tingting Wen'

import json
import smtplib
import traceback
import tweepy
import time
from NeCTAR.bucket import Bucket
from TwitterHarvester.TwitterHarvester.logger import Logger


class StreamingCollect(object):

    def __init__(self, auth, locations, username, ip_address):
        self.auth = auth
        self.locations = locations
        self.tweets_bucket = []
        self.username = username
        self.ip_address = ip_address

    def listen(self):
        l = StdOutListener(self.username)
        stream = tweepy.Stream(self.auth, l)
        stream.filter(locations=self.locations)



# This is the listener, responsible for receiving data
class StdOutListener(tweepy.StreamListener):

    def __init__(self, username, ip_address):
        super(self.__class__, self).__init__()
        self.tweets = []
        self.username = username
        self.bucket_size = 100
        self.bucket = Bucket.by_name('tweetdata')

        self.logger = Logger(self.username, 'log.txt')
        self.logger.log('Stalking has commenced...')
        self.ip_address = ip_address

    def on_data(self, data):
        try:
            # Twitter returns data in JSON format - we need to decode it first
            decoded = json.loads(data)
            # Also, we convert UTF-8 to ASCII ignoring all bad characters sent by users
            self.tweets.append(decoded)

            print len(self.tweets)
            if len(self.tweets) == self.bucket_size:
                self.send_bucket(self.tweets)
                self.tweets = []
        except:
            self.on_error('on_data_error', traceback.format_exc())
        return True

    def on_error(self, status, traceback = None):
        self.logger.log('STATUS: {}\n\nTRACEBACK:\n\n{}'.format(status, traceback))

        self.send_email(status)

    def send_bucket(self, data):
        self.logger.log('Sending bucket...')

        filename = self.username + '-' + str(int(time.time())) + '.json'
        json_string = json.dumps(data)
        self.bucket.add(filename, json_string)

    def send_email(self, status):
        self.logger.log('Sending email of status: {}'.format(status))

        msg = 'Error code (from {}) - {}'.format(self.ip_address, status)
        username = 'glasgowstalkers@gmail.com'
        password = 'qwer4141'
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(username, password)
        server.sendmail(username, 'zoe.wentingting@gmail.com', msg)
        server.close()