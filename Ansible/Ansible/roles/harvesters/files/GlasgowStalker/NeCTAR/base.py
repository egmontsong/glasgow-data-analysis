__author__ = 'shadyrafehi'

import boto
from boto.ec2.regioninfo import RegionInfo
import boto.s3.connection
from boto.s3.key import Key

from NeCTAR import Image

KEY_NAME = 'GlasgowStalkers'
ACCESS_KEY = 'de91ecdc6ddb4c0aaf1aca53c6a857f0'
SECRET_KEY = '591531c0bf484c6f864f1427f1d0915f'

class NeCTARBase(object):
    region = None
    connection = None
    default_key_name = KEY_NAME
    default_image = Image.ubuntu14_04
    default_security_groups = ['default']
    mapping = dict()

NeCTARBase.region = RegionInfo(name="melbourne", endpoint="nova.rc.nectar.org.au")
NeCTARBase.connection = boto.connect_ec2(aws_access_key_id=ACCESS_KEY,
                                         aws_secret_access_key=SECRET_KEY,
                                         is_secure=True,
                                         region=NeCTARBase.region,
                                         validate_certs=False,
                                         port=8773,
                                         path="/services/Cloud")


class NeCTARBaseS3(object):
    region = None
    connection = None
    mapping = dict()

NeCTARBaseS3.connection = boto.s3.connection.S3Connection(
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    port=8888,
    host='swift.rc.nectar.org.au',
    is_secure=True,
    validate_certs=False,
    calling_format=boto.s3.connection.OrdinaryCallingFormat())



