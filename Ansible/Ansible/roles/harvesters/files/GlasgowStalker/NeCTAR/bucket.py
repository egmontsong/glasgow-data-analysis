from boto.s3.key import Key
from NeCTAR.base import NeCTARBaseS3

__author__ = 'shadyrafehi'


class Item(NeCTARBaseS3):
    def __init__(self, file):
        self._file = file

    @property
    def name(self):
        return self._file.name

    def read(self):
        return self._file.get_contents_as_string()


class Bucket(NeCTARBaseS3):

    def __init__(self, bucket_id):
        self._map_id = bucket_id

    @property
    def _bucket(self):
        return super(self.__class__, self).mapping[self._map_id]

    @property
    def name(self):
        return self._bucket.name

    @property
    def items(self):
        return self.get_items(prefix=None)

    def get_items(self, prefix=None):
        return [Item(key) for key in self._bucket.list(prefix=prefix)]

    def refresh(self):
        Bucket.by_name(self.name, allow_cached=False)

    def add(self, key, value):
        item = Key(self._bucket)
        item.key = key
        item.set_contents_from_string(str(value))
        self.refresh()

    def remove(self, item):
        self._bucket.delete_key(item.name)

    @classmethod
    def all(cls):
        buckets = []
        for bucket in super(cls, cls).connection.get_all_buckets():
            super(cls, cls).mapping[bucket.name] = bucket
            buckets.append(cls(bucket.name))
        return buckets

    @classmethod
    def by_name(cls, name, allow_cached=True):
        if allow_cached:
            bucket = super(cls,cls).mapping.get(name)
            if bucket is not None:
                return cls(bucket.name)
        for bucket in cls.all():
            if bucket.name == name:
                return bucket

    @classmethod
    def new(cls, name):
        bucket_data = super(cls, cls).connection.create_bucket(name)
        super(cls,cls).mapping[bucket_data.name] = bucket_data
        return Bucket(bucket_data.name)

    def delete(self, force=False):
        if force:
            for item in self.items:
                self.remove(item)
        elif len(self.items) > 0:
            raise Exception('Trying to delete a bucket which is not empty (to force, add force=True)')
        super(self.__class__, self).connection.delete_bucket(self.name)

    def __repr__(self):
        return 'Bucket(Name={})'.format(self.name)
