__author__ = 'shadyrafehi'

import threading


class Events(object):
    pool = dict()

    @classmethod
    def wait(cls):
        import time
        event_count = -1
        while True:
            new_count = len(cls.pool)
            if new_count == 0:
                break
            if new_count != event_count:
                event_count = new_count
                print
                print 'Waiting for {} events to complete...'.format(event_count)
                for event in cls.pool:
                    print '  - {}'.format(cls.pool[event])
            time.sleep(0.1)


class Event (threading.Thread):
    def __init__(self, task, name = None):
        threading.Thread.__init__(self)

        from uuid import uuid4
        self.id = str(uuid4())
        self.name = name
        self.running = False
        self.task = task
        self.start()

    def run(self):
        self.running = True
        Events.pool[self.id] = self
        self.task()
        del Events.pool[self.id]
        self.running = False

    def wait(self):
        print
        print 'Waiting for {}'.format(self)
        while self.running:
            pass
        print

    def __repr__(self):
        return 'Event(Name={}, Id={})'.format(self.name, self.id)