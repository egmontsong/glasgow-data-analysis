__author__ = 'shadyrafehi'


import os


def process_id(filename):
    try:
        return int(open(filename, 'r').read())
    except:
        return 999999999


def is_process_running(pid):
    try:
        os.kill(pid, 0)
        return True
    except OSError:
        return False


def save_process(filename):
    with open(filename,'w') as f:
        import os
        f.write(str(os.getpid()))