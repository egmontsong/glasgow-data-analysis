__author__ = 'shadyrafehi'

import os
from process_manager import is_process_running, process_id

if is_process_running(process_id('harvester.pid')):
    print 'Harvester already running'
else:
    print 'Starting Harvester'
    os.system('nohup python main.py &')

if is_process_running(process_id('listener.pid')):
    print 'Listener already running'
else:
    print 'Starting Listener'
    os.system('nohup python listener.py &')
