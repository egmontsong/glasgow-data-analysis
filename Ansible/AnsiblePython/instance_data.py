__author__ = 'shadyrafehi'

from NeCTAR.instance import Instance
import json

parent = Instance.by_tag('name', 'Parent')[0]
children = Instance.by_tag('name', 'Harvester')

output = []
output.append(dict(group='parent', host=parent.ip_address, variables=dict(ansible_ssh_user='ubuntu')))
for i,child in enumerate(children, 1):
    variables = dict(index=i, parent=parent.ip_address, ansible_ssh_user='ubuntu')
    output.append(dict(group='harvester', host=child.ip_address, variables=variables))

print json.dumps(output)
