from NeCTAR import Size
from NeCTAR.event import Events
from NeCTAR.volume import Volume
from NeCTAR.instance import Instance
from NeCTAR.bucket import Bucket

CHILDREN = 4

"""
UNCOMMENT THE FOLLOWING ONLY IF YOU WANT TO DELETE ALL
CONTAINERS, VOLUMES AND INSTANCES
"""
# # Remove all buckets
# for bucket in Bucket.all():
#     bucket.delete(force=True)

# # Remove all existing volumes
# for volume in Volume.all():
#     volume.detach().wait()
#     volume.delete()
# Events.wait()

# # Remove all existing instances
# for instance in Instance.all():
#     instance.terminate()
# Events.wait()

# Create the bucket to hold the retrieve Twitter files
Bucket.new(name='tweetdata')

# Create one parent instance and four children instances
parent = Instance.new(name='Parent', size=Size.small)
children = []
for i in range(min(CHILDREN, 4)):
    children.append(Instance.new(name='Harvester', size=Size.small))
Events.wait()

Create a new volume and bind it to the parent
volume = Volume.new(size=40)
Events.wait()
parent.attach_volume(volume).wait()

