__author__ = 'shadyrafehi'


from timeprocessing import TimeProcessor, TimeRange, profile_pairer
import utm

def _subset(items, limit):
    from random import shuffle
    items = items[:]
    shuffle(items)
    return items[:limit]

def _cluster_points(points, n=2):
    import cluster
    import math
    clusterer = cluster.HierarchicalClustering(points, lambda a, b: math.hypot(a[0]-b[0], a[1]-b[1]))
    return sorted(clusterer.getlevel(100), key=len, reverse=True)[:n]

def _cluster_midpoint(cluster, utm_metadata):
    sum = [0, 0]
    for c in cluster:
        sum[0] += c[0]
        sum[1] += c[1]
    avg = [sum[0]/len(cluster), sum[1]/len(cluster)]
    #print avg
    return utm.to_latlon(*(avg+utm_metadata))

def _coord_dict(latlng):
    return {'lat': latlng[0], 'lng': latlng[1]}

def top_tweeters():
    from database import couch_database
    profile_db = couch_database.profiles_db
    profile_db.view('Test/test')
    return sorted([r.value for r in profile_db.view('Test/test')], key=lambda x: len(x['points']), reverse=True)

def geo_profile(tweeter_id):
    import utm
    from database import couch_database
    from timeprocessing import TimeProcessor

    profile_db = couch_database.profiles_db
    tweet_db = couch_database.tweet_db


    class Tweet(object):
        def __init__(self, latlng, tweet, time):
            self.latlng = latlng
            utm_list = utm.from_latlon(*latlng)
            self.utm = utm_list[:2]
            self.utm_metadata = utm_list[2:]
            self.tweet = tweet['text'].encode('utf-8')
            self.time = time

            current_sentiment = tweet['custom']['semantics']['polarity']
            if current_sentiment < -0.3:
                self.sentiment = 'negative'
            elif current_sentiment > 0.3:
                self.sentiment = 'positive'
            else:
                self.sentiment = 'neutral'

        @property
        def latlng_rounded(self):
            ndigit = 3
            offset = pow(10,3) / 2
            x = round(self.utm[0], -ndigit) + offset
            y = round(self.utm[1], -ndigit) + offset
            return utm.to_latlon(*((x, y)+self.utm_metadata))

    result = profile_db[tweeter_id]

    utm_metadata = list(utm.from_latlon(*result['points'][0]['coordinate'])[2:])
    tweets = [Tweet(row['coordinate'], tweet_db[row['tweet_id']], row['timestamp']) for i,row in enumerate(result['points'])]

    subset = _subset(tweets, 100)
    cluster_groups = _cluster_points([t.utm for t in subset], 2)
    overall_clusters = [_coord_dict(list(_cluster_midpoint(c, utm_metadata))) for c in cluster_groups]
    range_clusters = _timerange_clustering(tweets, utm_metadata)

    groups = TimeProcessor.group_by_dates(tweets, pairer=lambda t: t.time)

    data = []
    for group in groups:
        chunk = group['documents']
        chunk_subset = _subset(chunk, 50)
        cluster_groups = _cluster_points([t.utm for t in chunk_subset], 2)
        #print type(cluster_groups[0])
        if type(cluster_groups[0]) == tuple:
            cluster_groups = [cluster_groups]
       # print cluster_groups
        try:
            clusters = [list(_cluster_midpoint(c, utm_metadata)) for c in cluster_groups]
        except:
            #print ">>>", cluster_groups
            raise Exception('Killed {}'.format(cluster_groups))

        data.append({
            'points': [{'coord': _coord_dict(t.latlng), 'normCoord': _coord_dict(t.latlng_rounded),'time': dict(t.time.items()+[('days', group['date']['days'])]), 'tweet': t.tweet, 'sentiment': t.sentiment} for t in chunk],
            'clusters': [_coord_dict(c) for c in clusters],
            'metaData': {
                'date': group['date']
            }
        })



    geodata = {
        'clusters':overall_clusters,
        'rangeClusters': range_clusters,
        'data': data,
        'metaData': {
            'username': result['username'],
        }
    }

    user_info = tweet_db[result['points'][0]['tweet_id']]['user']
    userinfo = {
        'username': result['username'],
        'profile_image': user_info['profile_image_url'],
        'name': user_info['name'],
        'friends': user_info['friends_count'],
        'followers': user_info['followers_count'],
        'regions': {r['region'] for r in result['points']},
        'cluster_locations': [_find_location(c['lat'], c['lng']) for c in overall_clusters],
        'range_cluster_locations': {
            'home': [_find_location(c['lat'], c['lng']) for c in range_clusters['home']],
            'work': [_find_location(c['lat'], c['lng']) for c in range_clusters['work']]
        }
    }

    return {
        'data': geodata,
        'info': userinfo
    }

def _find_location(lat, lng):
    from pygeocoder import Geocoder, GeocoderError
    try:
        return str(Geocoder.reverse_geocode(lat, lng))
    except:
        return 'NA'

def _timerange_clustering(tweets, utm_metadata):
    time_ranges = [
        TimeRange(name='work_hours', start=8, end=18),
        TimeRange(name='home_hours_morning', start=0, end=7),
        TimeRange(name='home_hours_night', start=19, end=23)
    ]

    tweets = TimeProcessor.remove_dates(tweets, ['Saturday', 'Sunday'], pairer=lambda t: t.time)
    timerange_groups = TimeProcessor.group_by_hour_ranges(tweets, time_ranges, pairer=lambda t: t.time)
    timerange_dict = {r['name']: r for r in timerange_groups}
    home_tweets = timerange_dict['home_hours_morning']['documents'] + timerange_dict['home_hours_night']['documents']
    work_tweets = timerange_dict['work_hours']['documents']
    # work_tweets = TimeProcessor.remove_dates(work_tweets, ['Saturday', 'Sunday'], pairer=lambda t: t.time)

    cluster_groups = _cluster_points([t.utm for t in _subset(home_tweets, 50)], 1)
    home_clusters = [_coord_dict(list(_cluster_midpoint(c, utm_metadata))) for c in cluster_groups]

    cluster_groups = _cluster_points([t.utm for t in _subset(work_tweets, 50)], 1)
    work_clusters = [_coord_dict(list(_cluster_midpoint(c, utm_metadata))) for c in cluster_groups]

    return {'home': home_clusters, 'work': work_clusters}


if __name__ == '__main__':

    geo_profile('180460319')

    exit()

    from database import couch_database
    tweet_db = couch_database.tweet_db
    profile_db = couch_database.profiles_db
    profile_db.view('Test/test')

    results = sorted([r.value for r in profile_db.view('Test/test')], key=lambda x: len(x['points']), reverse=True)

    #print TimeProcessor.filter_by_date(results[0]['points'], 20150502, pairer=profile_pairer)
    #print TimeProcessor.group_by_dates(results[0]['points'], pairer=profile_pairer)
    #print TimeProcessor.filter_by_hour_range(results[0]['points'], 8, 10)

    user = results[0]['points']
    for user in results:
        user = user['points']
        time_ranges = [TimeRange('work_hours', 9, 17), TimeRange('lunch_hour', start=12, end=13)]
        groups = TimeProcessor.group_by_hour_ranges(user, time_ranges, pairer=profile_pairer)
        for group in groups:
            print 'range:{}, time range:{}, tweet count:{}'.format(group['name'], group['range'], len(group['documents']))