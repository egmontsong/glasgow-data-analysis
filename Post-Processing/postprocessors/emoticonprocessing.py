__author__ = 'Tianlong Song'
import database


class Emoticons_processing:

    def __init__(self):
        self.db = database.couch_database

    def get_all_emotions(self):
        all_emoticons = []
        for row in self.db.tweet_db.view('emotions/usage', group = True):
            all_emoticons.append([row.key,row.value])
        return all_emoticons

    def get_all_range(self):
        all_value = []
        all_range = []
        for row in self.db.tweet_db.view('emotions/range', group = True):
            all_value.append([row.key,row.value])
        for item in all_value:
            all_range.append([item[0],round(item[1]['min'],2),round(item[1]['max'],2)])
        return all_range

# e = Emoticons_processing()
# print e.get_all_range()
