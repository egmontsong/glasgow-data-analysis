__author__ = 'shadyrafehi'


class CouchDatabase(object):

    def __init__(self):
        import couchdb
        couch = couchdb.Server('http://127.0.0.1:{}'.format(5984))
        self.tweet_db = couch['tweets']
        self.profiles_db = couch['user_profiles']
        self.weather_db = couch['weather']

        # added by Charlene for testing
        #self.tweet_mini = couch['tweets_subset']


couch_database = CouchDatabase()
