__author__ = 'shadyrafehi'


from collections import namedtuple, defaultdict

DocumentTimePair = namedtuple('TweetTimePair', ['document', 'time'])
TimeRange = namedtuple('TimeRange', ['name', 'start', 'end'])

tweet_pairer = lambda d: d['custom']['time']
profile_pairer = lambda d: d['timestamp']

class TimeProcessor(object):

    @classmethod
    def pairer(cls, pairer, documents):
        return [DocumentTimePair(document=d, time=pairer(d)) for d in documents]

    @classmethod
    def filter_by_date(cls, documents, date, pairer=profile_pairer):
        if pairer:
            documents = cls.pairer(pairer, documents)

        return [d.document for d in documents if d.time['date'] == date]

    @classmethod
    def remove_dates(cls, documents, days, pairer=profile_pairer):
        if pairer:
            documents = cls.pairer(pairer, documents)

        return [d.document for d in documents if cls.date_dict(d.time['date'])['day'] not in days]

    @classmethod
    def date_dict(cls, date):
        from datetime import datetime
        epoch = datetime.utcfromtimestamp(0)
        date_obj = datetime(year=int(str(date)[:4]), month=int(str(date)[4:6]), day=int(str(date)[6:]))
        day = date_obj.strftime('%A')
        date_formatted = date_obj.strftime('%a, %-d %b')
        return dict(raw=date, day=day, formatted=date_formatted, days=(date_obj - epoch).days)


    @classmethod
    def group_by_dates(cls, documents, pairer=profile_pairer):
        if pairer:
            documents = cls.pairer(pairer, documents)

        output = defaultdict(list)

        for d in documents:
            date = d.time['date']
            output[date].append(d)

        return [
            dict(
                date=cls.date_dict(k),
                documents=[d.document for d in sorted(v, key=lambda d: (d.time['hour'], d.time['minute']))]
            )
            for k, v in sorted(output.items(), key=lambda r: r[0])
        ]



    @classmethod
    def filter_by_hour_range(cls, documents, start, end, pairer=profile_pairer):
        if pairer:
            documents = cls.pairer(pairer, documents)

        return [d.document for d in documents if start <= d.time['hour'] <= end]


    @classmethod
    def group_by_hour_ranges(cls, documents, ranges, pairer=profile_pairer):
        if pairer:
            documents = cls.pairer(pairer, documents)

        output = defaultdict(lambda: dict(range=[], documents=[]))
        for r in ranges:
            output[r.name]

        for d in documents:
            hour = d.time['hour']
            found_range = False
            for r in ranges:
                if r.start <= hour <= r.end:
                    output[r.name]['documents'].append(d)
                    output[r.name]['range'] = [r.start, r.end]
                    found_range = True
            if not found_range:
                output['other']['documents'].append(d)

        output_sorted = sorted(output.items(), key=lambda x:x[1]['range'])

        return [
            dict(
                name=k,
                range=v['range'],
                documents=[d.document for d in sorted(v['documents'], key=lambda d: (d.time['hour'],d.time['minute']))]
            )
            for k, v in output_sorted]

