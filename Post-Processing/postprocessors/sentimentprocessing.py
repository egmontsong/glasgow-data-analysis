__author__ = 'Tianlong Song'
import database


class Sentiment_processing:

    def __init__(self):
        self.db = database.couch_database

    def get_all_sentiment(self):
        all_sentiment = []
        max = 0
        for row in self.db.tweet_db.view('sentiment/group_distribution',group =True):

            if row.value > max:
                max = row.value
            all_sentiment.append([row.key[0],row.key[1],row.value])
        for i in all_sentiment:
            if i[2]==max:
                all_sentiment.remove(i)
        return all_sentiment

#
# sentiment_processing = Sentiment_processing()
# print "start"
# print sentiment_processing.get_all_sentiment()
