__author__ = 'charlene_chen'

import database
from couchdb.design import ViewDefinition
import couchdbviews
import json
import time
from os.path import exists


# using your views: design_document/views_name
def get_weekdayHashTags():
    db = database.couch_database.tweet_db
    results = [
        ['Mon', 0],
        ['Tue', 0],
        ['Wed', 0],
        ['Thu', 0],
        ['Fri', 0],
        ['Sat', 0],
        ['Sun', 0]
    ]
    for row in db.view('tweet_content/date_has',group = True):
        # _date = datetime.strptime(row.key[0], "%a %b %d %H:%M:%S +0000 %Y")
        # dt = _date.utctimetuple()
        # week_day = row.key[0].split(' ')[0]
        # count = len(row.value)
        # for x in results:
        #     if week_day == x[0]:
        #         x[1] += count

        _date = str(row.key)
        _date_array = [_date[:4], _date[4:6], _date[6:]]
        _date = '-'.join(_date_array)
        week_day = time.strptime(_date, "%Y-%m-%d").tm_wday
        # count = len(row.value)
        results[week_day][1] += row.value
        # results.append([week_day, row.value])

    return results

#
def count_media():
    db = database.couch_database.tweet_db
    results = []
    total = 0

    for row in db.view('tweet_content/tweet_media',group = True):
        total += row.value
        if row.key == 1:
            results.append(['NoMedia_count',row.value])
        if row.key == 2:
            results.append(['Media_count', row.value])
    # db_rows = len(db)
    # design_rows = 0
    # for row in db.view('_all_docs'):
    #     if row.id.startswith('_design'):
    #         design_rows += 1
    # # the number of rows except design documents
    # doc_rows = db_rows - design_rows
    # results[0][1] = (float(media_count) / doc_rows) * 100
    # results[1][1] = 100 - results[0][1]

    return results

# covert dict to json file
# def to_jsonFile(data):
#     json_str = json.dumps(data, ensure_ascii=False)
#     with open('./stalkerweb/static/dateHashTags.json', 'w') as f:
#         f.write(json_str)
#         f.close()
#
# def to_jsonFile(data, path):
#     json_str = json.dumps(data, ensure_ascii=False)
#     with open(path, 'w') as f:
#         f.write(json_str)
#         f.close()


#
# def main():
#     # using the local database for testing
#     # server = couchdb.Server()
#     # db = server[settings.database]
#
#     # create views in couchdb
#     # view = ViewDefinition('tweet_content', 'date_hashtags', couchdbviews.date_hashtag_mapper, language='python')
#     # view.sync(db)
#     #
#     # view_results = get_weekdayHashTags(db)
#     # # print view_results
#     #
#     # to_jsonFile(view_results)
#
#     # using remote database for testing
#     # db = database.couch_database.tweet_mini
#     # print db.info()
#
#     db = database.couch_database.tweet_db
#
#     # create views
#     view = ViewDefinition('tweet_content', 'date_hashtags', couchdbviews.date_hashtag_mapper, language='python')
#     view.sync(db)
#     view = ViewDefinition('tweet_content', 'tweet_media', couchdbviews.tweet_media_mapper, language='python')
#     view.sync(db)
#
#     view_results = get_weekdayHashTags(db)
#     # print view_results
#     # to_jsonFile(view_results, './stalkerweb/static/dateHashTags.json')
#
#     count_results = count_media(db)
#     # to_jsonFile(count_results, './stalkerweb/static/mediacount.json')





if __name__ == '__main__':

    print get_weekdayHashTags()
    print count_media()