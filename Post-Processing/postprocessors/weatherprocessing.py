__author__ = 'huangjun'

def get_weather():
    # import database
    from database import couch_database
    db = couch_database.tweet_db
    weather_db = couch_database.weather_db
    hours = []
    weather_hours = []
    days = []
    weather_days=[]
    i = 0
    # Open view
    for row in db.view('weather/hours', group=True):
        if row.key[0] == 20150423 or row.key[0] == 20150426:
            continue
        hours.append([row.key,row.value["sum"]/row.value["count"]])
        i += 1
        if i == 24:
            days.append(hours)
            hours = []
            i = 0

    i = 0
    day_names = set()
    for row in weather_db.view('weather/weather'):
        if row.key[0] == '2015-05-18':
            continue
        # print [row.key, row.value["weatherIconUrl"]]
        weather_hours.append(row.value["weatherIconUrl"])
        i += 1
        if i == 24:
            weather_days.append(weather_hours)
            weather_hours = []
            i = 0
            day_names.add(row.key[0])

    day_names = sorted(day_names)
    # print day_names

    return {"days": days, "symbol": weather_days, "day_names": day_names}


if __name__ == '__main__':
    get_weather()