__author__ = 'charlene_chen'


# map function
def date_hashtag_mapper(doc):
    # tweet by date+hashtag
    # if doc.get('created_at'):
    #     _date = doc['created_at']
    # else:
    #     _date = 0  # Jan 1 1970
    #
    # if doc.get('entities') and doc['entities'].get('hashtags'):
    #     # for hashtag in (doc['entities']['hashtags']):
    #     #     yield ([_date, doc['_id']], hashtag['text'].lower())
    #     yield ([_date, doc['_id']], doc['entities']['hashtags'])

    if doc.get('custom'):
        _date = doc['custom']['time']['date']
    else:
        _date = 0  # Jan 1 1970

    if doc.get('entities') and doc['entities'].get('hashtags'):
        # for hashtag in (doc['entities']['hashtags']):
        #     yield ([_date, doc['_id']], hashtag['text'].lower())
        yield ([_date, doc['_id']], doc['entities']['hashtags'])


def tweet_media_mapper(doc):
    # tweets whether has media
    if doc.get('entities') and doc['entities'].get('media'):
        yield (doc['_id'], len(doc['entities']['media']))
