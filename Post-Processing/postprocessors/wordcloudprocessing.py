__author__ = 'Tianlong Song'
import database


class Word_cloud_processor:
    all_words = []

    def __init__(self):
        self.db = database.couch_database

    def get_all_weather_words(self):

        all_words_string = ''
        for i in Word_cloud_processor.all_words:
            if i[0] != 'watch':
                a = (i[1]-i[1]*0.6+58)/58
                all_words_string += str(int(a))+ ' ' + i[0] + '\n'
        Word_cloud_processor.all_words = []
        return all_words_string


    def get_all_range(self):
        all_value = []
        all_average = []
        count = 0
        for row in self.db.tweet_db.view('word_cloud/all', group = True):
            all_value.append([row.key,row.value])
        for item in all_value:
            if item[0] != 'watch':
                # [word(key), count, average]
                Word_cloud_processor.all_words.append([item[0],item[1]['count'],round(item[1]['sum']/item[1]['count'],2)])
        from operator import itemgetter
        Word_cloud_processor.all_words.sort(key=itemgetter(1),reverse=True)
        for i in Word_cloud_processor.all_words:
            if (count<=30):
                all_average.append([i[0],i[2]])
            count +=1
        return all_average

# e = Word_cloud_processor()
# print e.get_all_range()
# print e.all_words
# print e.get_all_weather_words()

