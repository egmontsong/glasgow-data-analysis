__author__ = 'shadyrafehi'

import time
import os
from logger import Logger
from process_manager import save_process, process_id, is_process_running

save_process('listener.pid')

time.sleep(5)

while True:
    pid = process_id('harvester.pid')
    print is_process_running(pid)
    if not is_process_running(pid):
        print 'Not running!'
        Logger('Listener', 'log.txt').log('Listener forcing restart...')
        os.system('python run.py')
    else:
        print 'Running...'
    time.sleep(60)