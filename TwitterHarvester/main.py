__author__ = 'Tingting Wen'

import urllib2

import tweepy

from streamingCollect import StreamingCollect
from token import Token
from TwitterHarvester.TwitterHarvester.configReader import ConfigReader


from process_manager import save_process
save_process('harvester.pid')

config1 = ConfigReader("config.json")
config_dict = config1.get_config()
consumer_key = config_dict["consumer_key"]
consumer_secret = config_dict["consumer_secret"]
access_token = config_dict["access_token"]
access_token_secret = config_dict["access_token_secret"]
locations = config_dict["coordinates"]

try:
    ip_address = urllib2.urlopen('http://ip.42.pl/raw').read()
except:
    ip_address = None

token1 = Token(consumer_key, consumer_secret, access_token, access_token_secret)
auth = token1.createAuth()
username = tweepy.API(auth).me().name

stream_collect = StreamingCollect(auth, locations, username, ip_address)
stream_collect.listen()