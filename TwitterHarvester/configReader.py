__author__ = 'Tingting Wen'

import json


# Read lines from config file, to get tokens
class ConfigReader(object):

    def __init__(self, filename):
        self.filename = filename
        self.config = None

    def get_config(self):
        f = open(self.filename, buffering=1000, mode='r')
        self.config = json.loads(f.read())
        return self.config